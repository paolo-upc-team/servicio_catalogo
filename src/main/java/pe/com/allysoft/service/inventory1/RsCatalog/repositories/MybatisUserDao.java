package pe.com.allysoft.service.inventory1.RsCatalog.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pe.com.allysoft.service.inventory1.RsCatalog.mappers.UserMapper;
import pe.com.allysoft.service.inventory1.RsCatalog.mappers.UserMapper;
import pe.com.allysoft.service.inventory1.RsCatalog.models.User;
import pe.com.allysoft.service.inventory1.RsCatalog.models.User;

import java.util.List;

/**
 * Created by Paolo Ortega  on 4/02/2018.
 */
@Repository
public class MybatisUserDao implements UserDao {

    @Autowired
    UserMapper storeMapper;

    @Override
    public User getUser(User store) throws Exception {
        return storeMapper.getUser(store);
    }

    @Override
    public int createUser(User store) throws Exception {
        return storeMapper.createUser(store);
    }
}
