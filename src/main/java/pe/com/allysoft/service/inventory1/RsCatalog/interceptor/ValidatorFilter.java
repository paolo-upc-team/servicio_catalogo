package pe.com.allysoft.service.inventory1.RsCatalog.interceptor;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import pe.com.allysoft.service.inventory1.RsCatalog.models.User;
import pe.com.allysoft.service.inventory1.RsCatalog.models.rest.MyGenericResponse;
import pe.com.allysoft.service.inventory1.RsCatalog.utils.IdTxGenerator;
import pe.com.allysoft.service.inventory1.RsCatalog.utils.LogFormatter;
import pe.com.allysoft.service.inventory1.RsCatalog.utils.MyRequestWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class ValidatorFilter extends GenericFilterBean {

    private static final String SCHEMA = "User";
    private static final Logger LOGGER = LogManager.getLogger(ValidatorFilter.class);
    private static final String UUID_HEADER_ATTRIBUTE = "uuid-transaction";
    private static final String STATUS_ERROR = "error";

    public ValidatorFilter() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        LOGGER.info("doFilter");
        MyRequestWrapper myRequestWrapper = new MyRequestWrapper((HttpServletRequest) request);

        String body = myRequestWrapper.getBody();
        String uuidHeader = ((HttpServletRequest) request).getHeader(UUID_HEADER_ATTRIBUTE);
        LogFormatter.printLogBegin(LOGGER, null, body, null, uuidHeader);

        if (body == null || body.isEmpty()) {
            chain.doFilter(myRequestWrapper, response);
            return;
        }

        //validar json schema
        //if (validateSchema(body, response))
        chain.doFilter(myRequestWrapper, response);
    }

    public String convertObjectToJson(Object object) throws JsonProcessingException {
        if (object == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }
}
