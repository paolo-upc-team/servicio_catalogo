package pe.com.allysoft.service.inventory1.RsCatalog.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.allysoft.service.inventory1.RsCatalog.exception.ResourceException;
import pe.com.allysoft.service.inventory1.RsCatalog.models.User;
import pe.com.allysoft.service.inventory1.RsCatalog.models.rest.MyGenericResponse;
import pe.com.allysoft.service.inventory1.RsCatalog.service.UserService;
import pe.com.allysoft.service.inventory1.RsCatalog.utils.IdTxGenerator;
import pe.com.allysoft.service.inventory1.RsCatalog.utils.LogFormatter;

import javax.servlet.http.HttpServletResponse;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);
    private static final String STATUS_OK = "ok";
    private static final String STATUS_ERROR = "error";
    private static final String SYS_CODE_GENERIC_ERROR = "100";

    @Autowired
    private UserService userService;

    @PostMapping("/authenticate")
    public ResponseEntity<MyGenericResponse<User>> login(@RequestBody User user) {
        LOGGER.info("login(), user: {}", user);
        MyGenericResponse<User> response = new MyGenericResponse<>();
        try {
            userService.login(user);
            response.setStatus(STATUS_OK);
            response.setMessage("usuario autenticado con exito");
        } catch (ResourceException e) {
            return getResourceException(response, e);
        } catch (Exception e) {
            return getException(response, e);
        } finally {
            response.setMsgId(IdTxGenerator.getTransactionId());
            LogFormatter.printLogEnd(LOGGER, null, response, null);
        }
        return ResponseEntity.ok().body(response);
    }

    @PostMapping
    public ResponseEntity<MyGenericResponse<User>> createUser(@RequestBody User user,
                                                              HttpServletResponse response2) {
        LOGGER.info("createUser(), user: " + user);
        response2.addHeader("Access-Control-Allow-Origin", "*");
        response2.addHeader("Access-Control-Allow-Headers", "Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token");
        response2.addHeader("Access-Control-Allow-Methods", "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT");
        User userResult;
        MyGenericResponse<User> response = new MyGenericResponse<>();
        try {
            userResult = userService.createUser(user);
            response.setObjeto(userResult);
            if (userResult != null) response.setNumberRecords(1);
            response.setStatus(STATUS_OK);
        } catch (ResourceException e) {
            return getResourceException(response, e);
        } catch (Exception e) {
            return getException(response, e);
        } finally {
            response.setMsgId(IdTxGenerator.getTransactionId());
            LogFormatter.printLogEnd(LOGGER, null, response, null);
        }
        return ResponseEntity.ok().body(response);
    }

    private ResponseEntity<MyGenericResponse<User>> getResourceException(MyGenericResponse<User> response, ResourceException e) {
        LOGGER.error("error recurso detectado: ", e);
        response.setStatus(e.getHttpStatus().toString());
        response.setCode(e.getErrorCode());
        response.setMessage(e.getMessage());
        return ResponseEntity.status(e.getHttpStatus()).body(response);
    }

    private ResponseEntity<MyGenericResponse<User>> getException(MyGenericResponse<User> response, Exception e) {
        LOGGER.error("excepcion encontrada: ", e);
        response.setStatus(STATUS_ERROR);
        response.setCode(SYS_CODE_GENERIC_ERROR);
        response.setMessage(e.getMessage());
        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(response);
    }
}