package pe.com.allysoft.service.inventory1.RsCatalog.mappers;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;
import pe.com.allysoft.service.inventory1.RsCatalog.models.Store;
import pe.com.allysoft.service.inventory1.RsCatalog.models.User;

import java.util.List;

/**
 * Created by Paolo Ortega on 4/02/2018.
 */
@Mapper
@Component
public interface StoreMapper {

    @Results({
            @Result(property = "storeId", column = "store_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "price", column = "price"),
            @Result(property = "score", column = "score"),
            @Result(property = "isFavorite", column = "is_favorite"),
            @Result(property = "category", column = "category"),
            @Result(property = "url", column = "url")
    })
    @Select("<script>  select store_id,name,price,score,is_favorite,category, url " +
            "from store WHERE true " +
            "<if test=\"storeId != null\">and store_id=#{storeId}</if>" +
            "<if test=\"name != null\">and name=#{name}</if>" +
            "<if test=\"isFavorite != null\">and is_favorite=#{isFavorite}</if>" +
            "<if test=\"category != null\">and category=#{category}</if>" +
            "<if test=\"url != null\">and url=#{url}</if>" +
            "<if test=\"price != null\">and price=#{price}</if></script>")
    List<Store> listStore(Store store);

    @Results({
            @Result(property = "storeId", column = "store_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "price", column = "price"),
            @Result(property = "score", column = "score"),
            @Result(property = "isFavorite", column = "is_favorite"),
            @Result(property = "category", column = "category"),
            @Result(property = "url", column = "url")
    })
    @Select("<script>  select store_id,name,price,score,is_favorite,category, url " +
            "from store WHERE true " +
            "<if test=\"storeId != null\"> and store_id=#{storeId}</if></script>")
    Store getStore(Store store);

}
