package pe.com.allysoft.service.inventory1.RsCatalog.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.allysoft.service.inventory1.RsCatalog.models.Store;
import pe.com.allysoft.service.inventory1.RsCatalog.repositories.StoreDao;

import java.util.List;

/**
 * Created by Paolo Ortega  on 4/02/2018.
 */
@Service
public class StoreService {

    private static final Logger LOGGER = LogManager.getLogger(StoreService.class);

    @Autowired
    StoreDao storeDao;

    public Store getStore(Store store) throws Exception {
        Store store1;
        store1 = storeDao.getStore(store);
        //if(store1==null)store1=new User();
        return store1;
    }

    public List<Store> listStore(Store store) throws Exception {
        List<Store> stores;
        stores = storeDao.listStore(store);
        //if(stores==null)stores=new ArrayList<>();
        return stores;
    }
}
