package pe.com.allysoft.service.inventory1.RsCatalog.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pe.com.allysoft.service.inventory1.RsCatalog.exception.ResourceException;
import pe.com.allysoft.service.inventory1.RsCatalog.models.User;
import pe.com.allysoft.service.inventory1.RsCatalog.repositories.UserDao;

/**
 * Created by Paolo Ortega  on 4/02/2018.
 */
@Service
public class UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserService.class);

    @Autowired
    UserDao userDao;

    public User getUser(User user) throws Exception {
        User store1;
        store1 = userDao.getUser(user);
        //if(store1==null)store1=new User();
        return store1;
    }

    public void login(User user) throws Exception {
        if(user==null)throw  new ResourceException(HttpStatus.BAD_REQUEST,"usuario o contraseña estan vacios");
        User store1;
        store1 = getUser(user);
        LOGGER.info("store1: {}", store1);
        if(store1==null)throw  new ResourceException(HttpStatus.UNAUTHORIZED,"usuario o contraseña incorrectos");
    }

    public User createUser(User user) throws Exception {
        Integer rowsUpdate = userDao.createUser(user);
        LOGGER.info("rowsUpdate: " + rowsUpdate);
        return user;
    }
}
