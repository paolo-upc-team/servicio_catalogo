package pe.com.allysoft.service.inventory1.RsCatalog.models.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

//@JsonTypeName("myResponse")
//@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT ,use = JsonTypeInfo.Id.NAME)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"status", "msgId", "code", "message", "numberRecords", "objeto", "objetos"})
public class MyGenericResponse<E> {

    private String status;
    @JsonProperty(value = "msg_id")
    private String msgId;
    private String code;
    private String message;
    @JsonProperty(value = "number_records")
    private int numberRecords;
    private E objeto;
    private List<E> objetos;

    public E getObjeto() {
        return objeto;
    }

    public void setObjeto(E objeto) {
        this.objeto = objeto;
    }

    public List<E> getObjetos() {
        return objetos;
    }

    public void setObjetos(List<E> objetos) {
        this.objetos = objetos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNumberRecords() {
        return numberRecords;
    }

    public void setNumberRecords(int numberRecords) {
        this.numberRecords = numberRecords;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MyGenericResponse{" +
                "objeto=" + objeto +
                ", objetos=" + objetos +
                ", status='" + status + '\'' +
                ", numberRecords=" + numberRecords +
                ", msgId='" + msgId + '\'' +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
