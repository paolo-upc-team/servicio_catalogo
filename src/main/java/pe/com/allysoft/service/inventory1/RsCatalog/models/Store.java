package pe.com.allysoft.service.inventory1.RsCatalog.models;

/**
 * Created by Paolo Ortega on 23/02/2018.
 */
public class Store {
    private Integer storeId;
    private String name;
    private String price;
    private String score;
    private String isFavorite;
    private String category;
    private String url;

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Store{" +
                "storeId=" + storeId +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", score='" + score + '\'' +
                ", isFavorite='" + isFavorite + '\'' +
                ", userId=" + category +
                ", url='" + url + '\'' +
                '}';
    }
}
