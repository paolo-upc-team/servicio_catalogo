package pe.com.allysoft.service.inventory1.RsCatalog;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.TimeZone;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
		System.out.println("RsCatalogApplication - variable property SPRING_PROFILES_ACTIVE: "+System.getProperty("SPRING_PROFILES_ACTIVE"));
		//System.out.println("list properties: "+System.getProperties());
		TimeZone.setDefault(TimeZone.getTimeZone("America/Bogota"));
		return application.sources(RsCatalogApplication.class);
	}

}
