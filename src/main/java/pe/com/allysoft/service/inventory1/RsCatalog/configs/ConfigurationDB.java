package pe.com.allysoft.service.inventory1.RsCatalog.configs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Created by Paolo Ortega on 6/02/2018.
 */
@Configuration
@MapperScan("pe.com.allysoft.service.inventory1.RsCatalog.mappers")
public class ConfigurationDB {

    @Bean
    @ConfigurationProperties(prefix = "datasource")
    public DataSource userDataSource() {
        return DataSourceBuilder.create().build();
    }
}
