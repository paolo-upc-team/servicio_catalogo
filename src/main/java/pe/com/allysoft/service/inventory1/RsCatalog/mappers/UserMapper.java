package pe.com.allysoft.service.inventory1.RsCatalog.mappers;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;
import pe.com.allysoft.service.inventory1.RsCatalog.models.Store;
import pe.com.allysoft.service.inventory1.RsCatalog.models.User;

import java.util.List;

/**
 * Created by Paolo Ortega on 4/02/2018.
 */
@Mapper
@Component
public interface UserMapper {

    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "lastName", column = "lastName"),
            @Result(property = "email", column = "email"),
            @Result(property = "userName", column = "userName"),
            @Result(property = "password", column = "password"),
            @Result(property = "url", column = "url")
    })
    @Select("<script>  select user_id,name,lastName,email,userName,password " +
            "from user WHERE true " +
            "<if test=\"userId != null\">and user_id=#{userId}</if>" +
            "<if test=\"name != null\">and name=#{name}</if>" +
            "<if test=\"userName != null\">and userName=#{userName}</if>" +
            "<if test=\"password != null\">and password=#{password}</if>" +
            "</script>")
    User getUser(User user);

    @Insert("insert into user (name,lastName,email,userName,password) " +
            "values(#{name},#{lastName},#{email},#{userName},#{password})")
    @Options(useGeneratedKeys = true, keyProperty = "userId", keyColumn = "user_id")
    int createUser(User user);

}
