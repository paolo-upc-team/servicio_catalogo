package pe.com.allysoft.service.inventory1.RsCatalog.utils;

import org.apache.logging.log4j.ThreadContext;

import java.util.UUID;

/**
 * Created by portega on 25/07/2018.
 */
public class IdTxGenerator {
    private static final ThreadLocal<String> context = new ThreadLocal<String>();
    private static final String API_NAME = "RsCatalog";

    public static void startTransaction() {
        String idTx = UUID.randomUUID().toString();
        context.set(idTx);
        ThreadContext.put("uuid", idTx);
        ThreadContext.put("apiName", API_NAME);
        ThreadContext.put("startTime", ""+System.currentTimeMillis());
    }

    public static void startTransaction(String idTx) {
        if (idTx == null || idTx.isEmpty())
            idTx = UUID.randomUUID().toString();
        context.set(idTx);
        ThreadContext.put("uuid", idTx);
        ThreadContext.put("apiName", API_NAME);
        ThreadContext.put("startTime", ""+System.currentTimeMillis());
    }

    public static String getTransactionId() {
        return context.get();
    }

    public static void setTransactionId(String idTx) {
        context.set(idTx);
    }

    public static void endTransaction() {
        context.remove();
    }
}
