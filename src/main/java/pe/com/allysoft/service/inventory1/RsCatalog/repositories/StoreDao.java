package pe.com.allysoft.service.inventory1.RsCatalog.repositories;

import pe.com.allysoft.service.inventory1.RsCatalog.models.User;
import pe.com.allysoft.service.inventory1.RsCatalog.models.Store;

import java.util.List;

/**
 * Created by Paolo Ortega on 4/02/2018.
 */
public interface StoreDao {

    Store getStore(Store store) throws Exception;

    List<Store> listStore(Store store) throws Exception;
}
