package pe.com.allysoft.service.inventory1.RsCatalog.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pe.com.allysoft.service.inventory1.RsCatalog.mappers.StoreMapper;
import pe.com.allysoft.service.inventory1.RsCatalog.models.Store;

import java.util.List;

/**
 * Created by Paolo Ortega  on 4/02/2018.
 */
@Repository
public class MybatisStoreDao implements StoreDao {

    @Autowired
    StoreMapper storeMapper;

    @Override
    public Store getStore(Store store) throws Exception {
        return storeMapper.getStore(store);
    }

    @Override
    public List<Store> listStore(Store store) throws Exception {
        return storeMapper.listStore(store);
    }
}
