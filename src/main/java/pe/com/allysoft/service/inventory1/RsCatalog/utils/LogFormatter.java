package pe.com.allysoft.service.inventory1.RsCatalog.utils;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by Paolo Ortega on 07/02/2017.
 */
public class LogFormatter {

    public static final String API_NAME = "RsCatalog";

    public static void printLogBegin(Logger logger, String method, Object request, String mimeType, String uuid) {
        logger.debug("printLogBegin()");
        if (mimeType == null) mimeType = "application/json";
        IdTxGenerator.startTransaction(uuid);
        if (request == null) return;
        String formatted = "";
        if (mimeType.equals("application/json")) {
            formatted = JAXBStringFormatter.formatAsJson(request);
        } else if (mimeType.equals("application/xml")) {
            formatted = JAXBStringFormatter.formatAsXml(request);
        }
        logger.info("Initiating " + API_NAME);
        if (method == null) method = "";
        logger.info("REQUEST (" + API_NAME + ")" + method + ":\n" + formatted);
    }

    public static void printLogEnd(Logger logger, String method, Object response, String mimeType) {
        logger.debug("printLogEnd()");
        final long time = System.currentTimeMillis() - Long.valueOf(ThreadContext.get("startTime"));
        logger.info("transactionId: " + IdTxGenerator.getTransactionId());
        if (response == null) return;
        String formatted = "";
        if (mimeType == null) mimeType = "application/json";
        if (mimeType.equals("application/json")) {
            formatted = JAXBStringFormatter.formatAsJson(response);
        } else if (mimeType.equals("application/xml")) {
            formatted = JAXBStringFormatter.formatAsXml(response);
        }
        if (method == null) method = "";
        logger.info("RESPONSE " + method + ":\n" + formatted);
        logger.info("Total time (ms): " + time);
        logger.info("Finishing " + API_NAME);
        IdTxGenerator.endTransaction();
    }

    public static void printProxyRequestJson(Logger logger, String wsName, String method, String wsManagerUrl, Object request) {
        logger.info("API a usar: " + wsName + ", metodo: " + method + ", endPointAdress: " + wsManagerUrl);
        logger.info("Datos de entrada(" + wsName + "):\n" + JAXBStringFormatter.formatAsJson(request));
    }

    public static void printProxyRequest(Logger logger, String wsName, String method, String wsManagerUrl, String request) {
        logger.info("API a usar: " + wsName + ", metodo: " + method + ", endPointAdress: " + wsManagerUrl);
        logger.info("Datos de entrada:\n" + formatXml(request));
    }

    public static void printProxyResponseJson(Logger logger, String wsName, Object response) {
        logger.info("Response devuelto(" + wsName + "):\n" + JAXBStringFormatter.formatAsJson(response));
    }

    public static void printProxyResponse(Logger logger, String response) {
        logger.info("Response devuelto:\n" + formatXml(response));
    }

    public static String formatXml(String xml) {
        try {
            Transformer serializer = SAXTransformerFactory.newInstance().newTransformer();
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            //serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            //serializer.setOutputProperty("{http://xml.customer.org/xslt}indent-amount", "2");
            Source xmlSource = new SAXSource(new InputSource(new ByteArrayInputStream(xml.getBytes())));
            StreamResult res = new StreamResult(new ByteArrayOutputStream());
            serializer.transform(xmlSource, res);
            return new String(((ByteArrayOutputStream) res.getOutputStream()).toByteArray());
        } catch (Exception e) {
            return xml;
        }
    }

    public static class JAXBStringFormatter {

        public static String formatAsXml(Object jaxbobject) {
            if (jaxbobject == null) return "null";
            Log logger = LogFactory.getLog(JAXBStringFormatter.class);
            StringWriter writer = null;
            try {
                writer = new StringWriter();
                JAXBContext context = JAXBContext.newInstance(jaxbobject.getClass());
                Marshaller m = context.createMarshaller();
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                m.setProperty(Marshaller.JAXB_FRAGMENT, true);
                m.marshal(jaxbobject, writer);
            } catch (Exception e) {
                logger.error("There was an error printing object as XML: " + jaxbobject, e);
            }

            return writer.toString();
        }

        public static String formatAsJson(Object jaxbobject) {
            if (jaxbobject == null) return "nulo";
            if (jaxbobject instanceof String && jaxbobject.toString().isEmpty()) return "nulo";
            Log logger = LogFactory.getLog(JAXBStringFormatter.class);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            try {
                if (jaxbobject instanceof String) {
                    Object json = objectMapper.readValue(jaxbobject.toString(), Object.class);
                    return objectMapper.writeValueAsString(json);
                } else {
                    return objectMapper.writeValueAsString(jaxbobject);
                }
            } catch (JsonProcessingException e) {
                logger.error("There was an JsonProcessingException printing object as JSON: " + jaxbobject, e);
            } catch (IOException e) {
                logger.error("There was an IOException printing object as JSON: " + jaxbobject, e);
            }
            return null;
        }
    }
}
