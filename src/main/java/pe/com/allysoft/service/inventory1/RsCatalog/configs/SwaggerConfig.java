package pe.com.allysoft.service.inventory1.RsCatalog.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket AuthApi() {
        //Adding Header
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        aParameterBuilder.name("Authorization").modelRef(new ModelRef("string")).parameterType("header").required(true).build();
        List<Parameter> aParameters = new ArrayList<Parameter>();
        aParameters.add(aParameterBuilder.build());
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
                .select().apis(RequestHandlerSelectors.basePackage("pe/com/allysoft/service/inventory1/RsCatalog/controllers"))
                .paths(PathSelectors.any())
                .build().globalOperationParameters(aParameters);
    }

    @Bean
    public ApiInfo apiInfo() {
        return new ApiInfo(
                "Rest Authtentication API",
                "\"API documentation for security\"",
                "1.0.0",
                "Inquilima Solutions License Version 1.0",
                new Contact("Inquilima", "http://www.inquilima.com.pe/", "portega@inquilima.com.pe"),
                "Inquilima License Version 1.0", "http://www.inquilima.com.pe/", Collections.emptyList());
    }
}
