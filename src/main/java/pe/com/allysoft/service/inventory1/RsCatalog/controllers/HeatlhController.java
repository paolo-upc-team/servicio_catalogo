package pe.com.allysoft.service.inventory1.RsCatalog.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.allysoft.service.inventory1.RsCatalog.models.User;
import pe.com.allysoft.service.inventory1.RsCatalog.models.rest.MyGenericResponse;
import pe.com.allysoft.service.inventory1.RsCatalog.service.StoreService;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/health", produces = MediaType.APPLICATION_JSON_VALUE)
public class HeatlhController {

    private static final Logger LOGGER = LogManager.getLogger(HeatlhController.class);
    private static final String STATUS_OK = "ok";
    private static final String STATUS_ERROR = "error";
    private static final String SYS_CODE_GENERIC_ERROR = "100";

    @Autowired
    private StoreService storeService;

    @GetMapping
    public ResponseEntity<MyGenericResponse<User>> getHealth(HttpServletResponse response2) {
        LOGGER.info("getHealth");
        response2.addHeader("Access-Control-Allow-Origin", "*");
        response2.addHeader("Access-Control-Allow-Headers", "Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token");
        response2.addHeader("Access-Control-Allow-Methods", "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT");
        MyGenericResponse<User> response = new MyGenericResponse<>();
        return ResponseEntity.ok().body(response);
    }
}