package pe.com.allysoft.service.inventory1.RsCatalog.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.allysoft.service.inventory1.RsCatalog.exception.ResourceException;
import pe.com.allysoft.service.inventory1.RsCatalog.models.Store;
import pe.com.allysoft.service.inventory1.RsCatalog.models.rest.MyGenericResponse;
import pe.com.allysoft.service.inventory1.RsCatalog.service.StoreService;
import pe.com.allysoft.service.inventory1.RsCatalog.utils.IdTxGenerator;
import pe.com.allysoft.service.inventory1.RsCatalog.utils.LogFormatter;

import java.util.List;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestController
@RequestMapping(value = "/store", produces = MediaType.APPLICATION_JSON_VALUE)
public class StoreController {

    private static final Logger LOGGER = LogManager.getLogger(StoreController.class);
    private static final String STATUS_OK = "ok";
    private static final String STATUS_ERROR = "error";
    private static final String SYS_CODE_GENERIC_ERROR = "100";

    @Autowired
    private StoreService storeService;

    @GetMapping("/list")
    public ResponseEntity<MyGenericResponse<Store>> listStore(@RequestParam(value = "is_favorite", required = false) String isFavorite,
                                                              @RequestParam(value = "score", required = false) String score,
                                                              @RequestParam(value = "category", required = false) String category) {
        LOGGER.info("getUser(), serie_co: {}, serie_co: {}, category: {}", isFavorite, score, category);
        List<Store> storeList;
        MyGenericResponse<Store> response = new MyGenericResponse<>();
        Store storeReq = new Store();
        storeReq.setIsFavorite(isFavorite);
        storeReq.setScore(score);
        storeReq.setCategory(category);
        try {
            storeList = storeService.listStore(storeReq);
            LOGGER.info("storeList: {}", storeList);
            response.setObjetos(storeList);
            if (storeList != null) response.setNumberRecords(storeList.size());
            response.setStatus(STATUS_OK);
        } catch (ResourceException e) {
            return getResourceException(response, e);
        } catch (Exception e) {
            return getException(response, e);
        } finally {
            response.setMsgId(IdTxGenerator.getTransactionId());
            LogFormatter.printLogEnd(LOGGER, null, response, null);
        }
        return ResponseEntity.ok().body(response);
    }

    private ResponseEntity<MyGenericResponse<Store>> getResourceException(MyGenericResponse<Store> response, ResourceException e) {
        LOGGER.error("error recurso detectado: ", e);
        response.setStatus(STATUS_ERROR);
        response.setCode(e.getErrorCode());
        response.setMessage(e.getMessage());
        return ResponseEntity.status(e.getHttpStatus()).body(response);
    }

    private ResponseEntity<MyGenericResponse<Store>> getException(MyGenericResponse<Store> response, Exception e) {
        LOGGER.error("excepcion encontrada: ", e);
        response.setStatus(STATUS_ERROR);
        response.setCode(SYS_CODE_GENERIC_ERROR);
        response.setMessage(e.getMessage());
        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(response);
    }
}