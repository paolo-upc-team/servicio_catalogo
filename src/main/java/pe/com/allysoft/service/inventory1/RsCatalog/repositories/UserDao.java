package pe.com.allysoft.service.inventory1.RsCatalog.repositories;

import pe.com.allysoft.service.inventory1.RsCatalog.models.Store;
import pe.com.allysoft.service.inventory1.RsCatalog.models.User;

import java.util.List;

/**
 * Created by Paolo Ortega on 4/02/2018.
 */
public interface UserDao {

    User getUser(User store) throws Exception;

    int createUser(User store) throws Exception;

}
