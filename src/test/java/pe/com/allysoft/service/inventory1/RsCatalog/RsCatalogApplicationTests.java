package pe.com.allysoft.service.inventory1.RsCatalog;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pe.com.allysoft.service.inventory1.RsCatalog.models.Store;
import pe.com.allysoft.service.inventory1.RsCatalog.models.User;
import pe.com.allysoft.service.inventory1.RsCatalog.service.StoreService;
import pe.com.allysoft.service.inventory1.RsCatalog.service.UserService;
import pe.com.allysoft.service.inventory1.RsCatalog.utils.LogFormatter;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RsCatalogApplicationTests {

	private static final Logger LOGGER = LogManager.getLogger(RsCatalogApplicationTests.class);

	@Autowired
	StoreService storeService;
	@Autowired
	UserService userService;

	@Test
	public void contextLoads() {
	}

	@Test
	public void listStore(){
		List<Store> stores=new ArrayList<>();
		Store store=new Store();
		//store.setClient_id(1);
		//store.setNombres("paolo");

		try {
			stores= storeService.listStore(store);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		LOGGER.info("stores: "+stores);
	}

	@Test
	public void getUser(){
		User user1=new User();
		User user=new User();
		user.setUserName("paolo");
		//user.setNombres("paolo");

		try {
			user1= userService.getUser(user);

			LOGGER.info(LogFormatter.JAXBStringFormatter.formatAsJson(user1));
		} catch (Exception e) {
			LOGGER.error(e);
		}
		LOGGER.info("user1: "+user1);
	}

	@Test
	public void login(){
		User user=new User();
		user.setUserName("jsanchez");
		user.setPassword("123456");

		try {
			userService.login(user);
			LOGGER.info("paso login");
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@Test
	public void createUser(){
		User user=new User();
		user.setUserName("user_prueba2");
		user.setEmail("user_prueba2@prueba.com");
		user.setPassword("123456");
		user.setName("user_prueba2");
		user.setLastName("user_prueba2");

		LOGGER.info(LogFormatter.JAXBStringFormatter.formatAsJson(user));

		try {
			user= userService.createUser(user);
		} catch (Exception e) {
			LOGGER.error("", e);
		}
		LOGGER.info("user: "+user);
	}

}
