FROM openjdk:8-jre-alpine
MAINTAINER Paolo Ortega <paolosp29@gmail.com>
RUN mkdir -p /javaConfig/
RUN mkdir -p /javaLog/
VOLUME ["/javaConfig/"]
VOLUME ["/javaLog/"]
ADD target/RsCatalog.jar RsCatalog.jar
ENTRYPOINT ["java", "-jar", "/RsCatalog.jar"]
EXPOSE 8081